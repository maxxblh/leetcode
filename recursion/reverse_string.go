package main

import "fmt"

func main() {
	inputStringOne := []string{"h","e","l","l","o"}
	inputStringTwo := []string{"H","a","n","n","a","h"}

	outputStringOne := reverseString(inputStringOne)
	fmt.Println(outputStringOne)

	outputStringTwo := reverseString(inputStringTwo)
	fmt.Println(outputStringTwo)
}

func reverseString(s []string) []string {
	return recursive(s, 0, len(s) - 1)
}

func recursive(s []string, left int, right int) []string {
	if left > right {
		return s
	}
	temp := s[left]
	s[left] = s[right]
	s[right] = temp
	return recursive(s, left + 1, right - 1)
}