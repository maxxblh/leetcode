package main

import "fmt"

func main() {
	output := fib(4)
	fmt.Println(output)
}

func fib(n int) int {
    if n < 2 {
		return n
	}
	return fib(n -1) + fib(n - 2)
}