package main

import "fmt"

func main() {
	output := getRow(6)
	fmt.Println(output)
}

func getRow(rowIndex int) []int {
    if rowIndex == 0 {
		return []int{1}
	}
	row := getRow(rowIndex - 1)
	calcRow := []int{row[0]}
	for i, value := range row {
		item := 1
		if len(row) != i + 1 {
			item = value + row[i + 1]
		}
		calcRow = append(calcRow, item)

	}
	return calcRow
}