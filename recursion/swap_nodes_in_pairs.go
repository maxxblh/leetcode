package main

import "fmt"

func main() {
  head := createList(4)
  swapped := swapPairs(head)

  fmt.Println(swapped)
}

func swapPairs(head *ListNode) *ListNode {
 if head == nil || head.Next == nil {
    return head
  }
  swapped := swapPairs(head.Next.Next)
  result := head.Next
  result.Next = head
  head.Next = swapped
  return result
}

func createList(amountEl int) *ListNode {
	list := createNode(1, amountEl)
	return list
}

func createNode(firstVal int, amountVal int) *ListNode {
	node := ListNode{ Val: firstVal }
	if firstVal == amountVal {
		return &node
	}
	node.Next = createNode(firstVal + 1, amountVal)
	return &node
}

type ListNode struct {
  Val int
  Next *ListNode
}
