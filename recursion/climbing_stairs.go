package main

import "fmt"

var hashMap = make(map[int]int)

func main() {
	output := climbStairs(3)
	fmt.Println(output)
}

func climbStairs(n int) int {
	if val, ok := hashMap[n]; ok {
		return val
	}

	if n == 1 || n == 0 {
		return 1
	}

	hashMap[n] = climbStairs(n-1) + climbStairs(n-2);

	return hashMap[n]
}
