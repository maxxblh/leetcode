package main

import "fmt"

func main() {
	head := createList(5)
	reversed := reverseList(head)
	fmt.Println(reversed)
}

func reverseList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	shifted := reverseList(head.Next)

	temp := shifted
	for temp.Next != nil {
		temp = temp.Next
	}
	result := shifted
	head.Next = nil
	temp.Next = head

	return result
}

func createList(amountEl int) *ListNode {
	list := createNode(1, amountEl)
	return list
}

func createNode(firstVal int, amountVal int) *ListNode {
	node := ListNode{Val: firstVal}
	if firstVal == amountVal {
		return &node
	}
	node.Next = createNode(firstVal+1, amountVal)
	return &node
}

type ListNode struct {
	Val  int
	Next *ListNode
}
