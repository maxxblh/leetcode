package main

import "fmt"

func main() {
	root := createTreeNode()
	result := searchBST(&root, 2)
	fmt.Println(result)
}

 func searchBST(root *TreeNode, val int) *TreeNode {
	 if root.Val == val {
		 return root
	 }
	 if root.Left != nil {
		leftNode := searchBST(root.Left, val)
		if leftNode != nil {
			return leftNode
		}
	 }
	 if root.Right != nil {
		rightNode := searchBST(root.Right, val)
		if rightNode != nil {
			return rightNode
		}
	 }
	 return nil
}

func createTreeNode() TreeNode {
	oneNode := TreeNode{ Val: 1 }
	threeNode := TreeNode{ Val: 3 }
	twoNode := TreeNode{ Val: 2, Left: &oneNode, Right: &threeNode }
	sevenNode := TreeNode{ Val:7 }
	root := TreeNode{ Val: 4, Left: &twoNode, Right: &sevenNode }

	return root
}

type TreeNode struct {
	Val int
	Left *TreeNode
	Right *TreeNode
}